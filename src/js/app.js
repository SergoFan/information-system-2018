import Vue from 'vue';
import VueRouter from 'vue-router';

import App from '~/components/App.vue';

import routes from '~/routes';

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes: routes,
});

const app = new Vue({
    router,
    ...App,
})